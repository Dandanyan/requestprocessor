package com.ingenico.processmessage.utils

object Constants {

    /**
     * Special name for Database file where the request is kept
     * */
    const val DB_FILE_NAME = "requestsDB.txt"

    /**
     * Delay period
     * */
    const val DELAY_PERIOD = 300L

    /**
     * Max clients count
     * */
    const val MAX_CLIENT_COUNT = 10
}