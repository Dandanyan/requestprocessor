package com.ingenico.processmessage.model.sender

import android.util.Log
import com.ingenico.processmessage.model.dao.Request
import com.ingenico.processmessage.model.reciever.MessageRinTimeRepo
import kotlinx.coroutines.*

/**
 * Client who sends request periodically to the Server
 * @param clientID unique id: each client must have unique id
 * */
class Client(private val clientID: Int) {

    private val TAG = Client::class.simpleName

    /**
     * periodically sends request to the server
     * @param timeInterval delay time until next request
     * */
    fun repeatRequest(timeInterval:Long, coroutineScope: CoroutineScope): Client {
      coroutineScope.launch{
            while (isActive ){
                //generate Request priority 0 – 255 (0 – the highest priority)
                val priority =  (0..255).random()
                Log.i(TAG, "Request is sending by $clientID ")

                sendRequest(Request(clientID, priority, System.currentTimeMillis(), "Same Data"))
                delay(timeInterval)
            }
        }

        return this
    }

    /**
     * Intended for request sending
     * @param request the request which should be send. Learn more {@link Request}
     * */
    private fun sendRequest(request: Request){
        MessageRinTimeRepo.addRequest(request)
    }
}