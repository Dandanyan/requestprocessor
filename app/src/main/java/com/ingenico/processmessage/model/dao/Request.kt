package com.ingenico.processmessage.model.dao

/**
 * Intended for containing request information which is used by client and server
 * */
data class Request(
    /**
     * Client unique id
     * */
    val clientId:Int,
    /**
     * Request priority 0 – 255 (0 – the highest priority)
     * */
    val priority:Int,
    /**
     * Time when request is forming in system ticks
     * */
    val requestFormingTime:Long,
    /**
     * Abstract data
     * */
    val msg:String)