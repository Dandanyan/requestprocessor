package com.ingenico.processmessage.model.fetcher

import android.util.Log
import com.ingenico.processmessage.model.dao.Request
import com.ingenico.processmessage.model.reciever.MessageRinTimeRepo
import com.ingenico.processmessage.utils.Constants
import kotlinx.coroutines.*
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.io.IOException

/**
 * This class is intended for reading data from cash data base and store it to the file
 * @param filesDir dir where file database file will be stored: internal file
 * */
class Server(filesDir: File) {
    private val TAG = Server::class.simpleName
    var file: File = File(filesDir, Constants.DB_FILE_NAME)
    var buf: BufferedWriter = BufferedWriter(FileWriter(file))
    private lateinit var mJob:Job

    /**
     * Send request repeatedly until user cancels it
     * @param timeInterval the interval for delaying until the next request
     * */
    fun repeatFetchRequest(timeInterval: Long) {
        mJob = CoroutineScope(Dispatchers.IO).launch {
            while (isActive) {
                val request = MessageRinTimeRepo.getAndInvalidateRequestByPriority()
                Log.i(
                    TAG,
                    "....Fetching Request \n ${request.priority}\n requested time: ${request.requestFormingTime}\n requestFetchingTime: ${System.currentTimeMillis()} "
                )
                writeRequestToFile(request)
                delay(timeInterval)
            }
        }
    }

    /**
     * It Writes request in the file
     * @param request request which should be kept in the file
     * */
    private fun writeRequestToFile(request: Request?) {

        try {
            if (request == null){
                return
            }
            if (!file.canWrite()){
                buf = BufferedWriter(FileWriter(file))
            }
            buf.append("${System.currentTimeMillis()} -> $request")
            buf.newLine()
        } catch (e: IOException) {
            Log.e(TAG, "File write failed: $e")
        }
    }

    /**
     * Cancels process operations
     * */
    fun cancelProcessing(){
        try {
            buf.close()
        } catch (e: IOException) {
            Log.e("Exception", "DB connection is closed: $e")
        }

        mJob.cancel()
    }

}