package com.ingenico.processmessage.model.reciever

import com.ingenico.processmessage.model.dao.Request
import java.util.concurrent.PriorityBlockingQueue
import kotlin.Comparator

/**
 * This class is intended for interaction with chased DB
 * */
object MessageRinTimeRepo {
    /**
     * As request must be ordered by priority, special comparator is created
     * */
    private val compareByLength: Comparator<Request> = compareBy { it.priority }
    /**
     * DB where the requests of the client should be kept temporarily
     * The Client will add the request to this cash db and correspondingly the server will fetch the requests from it
     * */
    private val requestsDB = PriorityBlockingQueue(100, compareByLength)

    /**
     * Adds Request to the DB
     * @param request contains request data which will be kept in the DB file, you can learn more about request {@Link Request}
     * */
    fun addRequest(request: Request){
        requestsDB.add(request)
    }

    /**
     * Gets and invalidates the request from cache
     * @return prioritised request, you can learn more about request {@Link Request}
     * */
    fun getAndInvalidateRequestByPriority(): Request {
        return requestsDB.take()
    }
}