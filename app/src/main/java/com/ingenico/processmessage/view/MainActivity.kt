package com.ingenico.processmessage.view

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.ingenico.processmessage.model.sender.Client
import com.ingenico.processmessage.R
import com.ingenico.processmessage.model.fetcher.Server
import com.ingenico.processmessage.databinding.ActivityMainBinding
import com.ingenico.processmessage.utils.Constants

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MessageProcessingViewModel
    private val clientsLIst = ArrayList<Client>()
    private var isProcessStarted = false
    private lateinit var binding: ActivityMainBinding
    private lateinit var server: Server

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this).get(MessageProcessingViewModel::class.java)
        server = Server(this.filesDir)

        binding.btnStartStop.setOnClickListener {
            val strCount = binding.etClientCount.text.toString()
            //user cancels operation
            if (isProcessStarted) {
                binding.btnStartStop.text = resources.getString(R.string.btn_start)
                isProcessStarted = false
                binding.processIndicator.visibility = View.GONE
                viewModel.cancelClientsOperation()

                server.cancelProcessing()
            } else {
                //inputType of the EditText is number which is selected in the xml,
                // so the value of editText can be converted to Int without exception
                if (strCount.isEmpty() || binding.etClientCount.text.toString()
                        .toInt() > Constants.MAX_CLIENT_COUNT
                ) {
                    Toast.makeText(
                        this,
                        resources.getString(R.string.client_count_error_message),
                        Toast.LENGTH_SHORT
                    ).show()
                    clientsLIst.clear()

                    return@setOnClickListener
                }

                isProcessStarted = true
                binding.btnStartStop.text = resources.getString(R.string.btn_cancel)
                viewModel.generateClients(strCount.toInt())
                viewModel.startSendingRequestPeriodically(Constants.DELAY_PERIOD)
                server.repeatFetchRequest(Constants.DELAY_PERIOD)
                binding.processIndicator.visibility = View.VISIBLE
            }
        }
    }
}