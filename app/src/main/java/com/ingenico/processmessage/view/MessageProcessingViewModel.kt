package com.ingenico.processmessage.view

import androidx.lifecycle.ViewModel
import com.ingenico.processmessage.model.sender.Client
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel

class MessageProcessingViewModel: ViewModel() {

    /**
     * Coroutine scope for running and handling
     **/
    private val coroutineScope:CoroutineScope = CoroutineScope(Job() + Dispatchers.IO)

    /**
     * Container for keeping generated users
     * */
    private val clientsList = ArrayList<Client>()

    /**
     * Generates number of Clients
     * @param clientsCount clients count
     * */
    fun generateClients(clientsCount:Int){
        for (index in 0..clientsCount){
            clientsList.add(Client(index))
        }
    }

    /**
     * Starts sending request periodically
     * */
    fun startSendingRequestPeriodically(periodOfTime: Long){
        for (client in clientsList){
            client.repeatRequest(periodOfTime, coroutineScope)
        }
    }

    /**
     * cancels message sending operation for all users
     * */
    fun cancelClientsOperation(){
        coroutineScope.cancel()
    }
}